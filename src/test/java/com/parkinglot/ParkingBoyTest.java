package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car_and_parkingBoy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //when
        Car fetchCar = parkingBoy.fetch(parkingTicket);
        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_exception_message_with_error_when_park_given_full_parking_lot_and_car_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        parkingLot.setRemainingCapacity(1);
        Car car1 = new Car();
        parkingBoy.park(car1);
        Car car2 = new Car();
        //when
        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(car2));
        assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_return_two_right_car_when_fetch_given_parking_lot_and_two_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car coolCar = new Car();
        Car notCoolCar = new Car();
        ParkingTicket parkingTicket1 = parkingBoy.park(coolCar);
        ParkingTicket parkingTicket2 = parkingBoy.park(notCoolCar);
        //when
        Car fetchCoolCar = parkingBoy.fetch(parkingTicket1);
        Car fetchNotCoolCar = parkingBoy.fetch(parkingTicket2);
        //then
        assertEquals(coolCar, fetchCoolCar);
        assertEquals(notCoolCar, fetchNotCoolCar);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_wrong_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket wrongTicket = new ParkingTicket(new Car());
        //when
        //then
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_used_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket parkingTicket = parkingBoy.park(new Car());
        parkingBoy.fetch(parkingTicket);
        //when
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(parkingTicket));
        //then
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_parking_ticket_from_first_lot_when_park_given_two_parking_lots_and_car_and_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //then
        assertTrue(parkingLot1.getTicketList().contains(parkingTicket));
        assertFalse(parkingLot2.getTicketList().contains(parkingTicket));
    }
    
    @Test
    void should_return_ticket_from_second_lot_when_park_given_two_lots_and_one_full_and_car_and_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        parkingLot1.park(new Car());
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //then
        assertTrue(parkingLot2.getTicketList().contains(parkingTicket));
        assertFalse(parkingLot1.getTicketList().contains(parkingTicket));
    }

    @Test
    void should_return_two_right_caren_fetch_given_two_lots_both_with_car_and_two_tickets_and_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket ticket1 = parkingLot1.park(car1);
        ParkingTicket ticket2 = parkingLot2.park(car2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        //when
        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);
        //then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
    }
    
    @Test
    void should_throw_exception_with_error_message_when_fetch_given_two_lots_and_parking_boy_and_unrecognized_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket unrecognizedTicket = new ParkingTicket(new Car());
        //when
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(unrecognizedTicket));
        //then
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_two_lots_and_parking_boy_and_used_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingTicket parkingTicket1 = parkingLot1.park(new Car());
        ParkingTicket parkingTicket2 = parkingLot2.park(new Car());
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        parkingBoy.fetch(parkingTicket1);
        parkingBoy.fetch(parkingTicket2);
        //when
        var exception1 = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(parkingTicket1));
        var exception2 = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetch(parkingTicket2));
        //then
        assertEquals("Unrecognized parking ticket", exception1.getMessage());
        assertEquals("Unrecognized parking ticket", exception2.getMessage());
    }
    
    @Test
    void should_throw_exception_with_error_message_when_park_given_two_lots_without_position_and_car_and_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        Car car = new Car();
        //when
        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(car));
        assertEquals("No available position", exception.getMessage());
    }
}
