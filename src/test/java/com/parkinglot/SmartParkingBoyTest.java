package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SmartParkingBoyTest {
    @Test
    void should_return_parking_ticket_from_more_position_lot_when_park_given_car_and_smart_parking_boy_and_two_different_remain_position_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);
        //then
        assertTrue(parkingLot2.getTicketList().contains(parkingTicket));
        assertFalse(parkingLot1.getTicketList().contains(parkingTicket));
    }
}
