package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingLot.park(car);
        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        //when
        Car fetchCar = parkingLot.fetch(parkingTicket);
        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_exception_message_with_error_when_park_given_full_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setRemainingCapacity(1);
        Car car1 = new Car();
        parkingLot.park(car1);
        Car car2 = new Car();
        //when
        //then
        var exception = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car2));
        assertEquals("No available position", exception.getMessage());
    }
    
    @Test
    void should_return_two_right_car_when_fetch_given_parking_lot_and_two_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car coolCar = new Car();
        Car notCoolCar = new Car();
        ParkingTicket parkingTicket1 = parkingLot.park(coolCar);
        ParkingTicket parkingTicket2 = parkingLot.park(notCoolCar);
        //when
        Car fetchCoolCar = parkingLot.fetch(parkingTicket1);
        Car fetchNotCoolCar = parkingLot.fetch(parkingTicket2);
        //then
        assertEquals(coolCar, fetchCoolCar);
        assertEquals(notCoolCar, fetchNotCoolCar);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket wrongTicket = new ParkingTicket(new Car());
        //when
        //then
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(new Car());
        parkingLot.fetch(parkingTicket);
        //when
        var exception = assertThrows(UnRecognizedParkingTicketException.class, () -> parkingLot.fetch(parkingTicket));
        //then
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }
}
