package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_ticket_from_more_available_position_rate_when_park_given_super_smart_parking_boy_and_car_and_two_different_available_parking_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(20);
        parkingLot1.park(new Car());
        for (int i = 0; i < 9; i++) {
            parkingLot2.park(new Car());
        }
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = superSmartParkingBoy.park(car);
        //then
        assertTrue(parkingLot1.getTicketList().contains(parkingTicket));
        assertFalse(parkingLot2.getTicketList().contains(parkingTicket));
    }
}
