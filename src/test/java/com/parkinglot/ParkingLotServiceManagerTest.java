package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnRecognizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotServiceManagerTest {
    @Test
    void should_return_true_ticket_when_park_or_fetch_given_manager_and_some_boys_and_some_lots_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(20);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingLot parkingLot3 = new ParkingLot(20);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot3);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot2, parkingLot3);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot2);
        //when
        parkingLotServiceManager.addParkingBoy(parkingBoy, smartParkingBoy, superSmartParkingBoy);
        ParkingTicket parkingTicketStandard = parkingLotServiceManager.
                specifyBoyPark(parkingLotServiceManager.getParkingBoyList().get(0), car1);
        ParkingTicket parkingTicketSmart = parkingLotServiceManager.
                specifyBoyPark(parkingLotServiceManager.getParkingBoyList().get(1), car2);
        parkingLot2.park(new Car());
        ParkingTicket parkingTicketSuperSmart = parkingLotServiceManager.
                specifyBoyPark(parkingLotServiceManager.getParkingBoyList().get(2), car3);
        //then
        assertTrue(parkingLot1.getTicketList().contains(parkingTicketStandard));
        assertTrue(parkingLot3.getTicketList().contains(parkingTicketSmart));
        assertTrue(parkingLot3.getTicketList().contains(parkingTicketSuperSmart));
        //when
        Car fetchCar1 = parkingLotServiceManager.
                specifyBoyFetch(parkingLotServiceManager.getParkingBoyList().get(1), parkingTicketStandard);
        Car fetchCar2 = parkingLotServiceManager.
                specifyBoyFetch(parkingLotServiceManager.getParkingBoyList().get(2), parkingTicketSmart);
        Car fetchCar3 = parkingLotServiceManager.
                specifyBoyFetch(parkingLotServiceManager.getParkingBoyList().get(2), parkingTicketSuperSmart);
        //then
        assertEquals(car1, fetchCar1);
        assertEquals(car2, fetchCar2);
        assertEquals(car3, fetchCar3);
    }

    @Test
    void should_return_ticket_or_car_when_park_or_fetch_given_manager_and_car_and_parking_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingLot parkingLot3 = new ParkingLot(20);
        parkingLot1.park(new Car());
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingLotServiceManager.park(car);
        //then
        assertTrue(parkingLot2.getTicketList().contains(parkingTicket));
        assertFalse(parkingLot3.getTicketList().contains(parkingTicket));
        //when
        Car fetchCar = parkingLotServiceManager.fetch(parkingTicket);
        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_unrecognized_ticket_manager_boy_and_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.addParkingBoy(parkingBoy);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLotServiceManager.getParkingBoyList().get(0).park(car);
        ParkingTicket unrecognizedTicket = new ParkingTicket(new Car());
        //when
        var exception = assertThrows(UnRecognizedParkingTicketException.class,
                () -> parkingLotServiceManager.getParkingBoyList().get(0).fetch(unrecognizedTicket));
        //then
        assertEquals("Unrecognized parking ticket", exception.getMessage());
        assertTrue(parkingLot.getTicketList().contains(parkingTicket));
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_used_ticket_manager_boy_and_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.addParkingBoy(parkingBoy);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLotServiceManager.getParkingBoyList().get(0).park(car);
        parkingLotServiceManager.getParkingBoyList().get(0).fetch(parkingTicket);
        //when
        var exception = assertThrows(UnRecognizedParkingTicketException.class,
                () -> parkingLotServiceManager.getParkingBoyList().get(0).fetch(parkingTicket));
        //then
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_car_manager_boy_and_full_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.addParkingBoy(parkingBoy);
        Car car = new Car();
        parkingLotServiceManager.getParkingBoyList().get(0).park(car);
        //when
        var exception = assertThrows(NoAvailablePositionException.class,
                () -> parkingLotServiceManager.getParkingBoyList().get(0).park(car));
        //then
        assertEquals("No available position", exception.getMessage());
    }
}
