package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SmartParkingBoy extends ParkingBoy{
    public SmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car) {
        return Arrays.stream(getParkingLot()).filter(parkingLot -> parkingLot.getRemainingCapacity() > 0)
                .max(Comparator.comparingInt(ParkingLot::getRemainingCapacity)).orElse(getParkingLot()[0]).park(car);
    }
}
