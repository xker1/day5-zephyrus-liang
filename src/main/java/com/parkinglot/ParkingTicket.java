package com.parkinglot;

public class ParkingTicket {
    private Car car;
    private boolean isUsed;

    public ParkingTicket(Car car) {
        this.car = car;
        this.isUsed = false;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }
}
