package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SuperSmartParkingBoy extends SmartParkingBoy{
    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car) {
        return Arrays.stream(getParkingLot()).filter(parkingLot -> parkingLot.getRemainingCapacity() > 0)
                .max(Comparator.comparingDouble(parkingLot -> parkingLot.getRemainingCapacity() / (double) parkingLot.getTotalCapacity()))
                .orElse(getParkingLot()[0]).park(car);
    }
}
