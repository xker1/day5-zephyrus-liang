package com.parkinglot.Exception;

public class UnRecognizedParkingTicketException extends RuntimeException{
    public UnRecognizedParkingTicketException() {
        super("Unrecognized parking ticket");
    }
}
