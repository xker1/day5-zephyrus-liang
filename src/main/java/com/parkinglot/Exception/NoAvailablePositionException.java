package com.parkinglot.Exception;

public class NoAvailablePositionException extends RuntimeException{
    public NoAvailablePositionException() {
        super("No available position");
    }
}
