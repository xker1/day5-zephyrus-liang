package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnRecognizedParkingTicketException;

import java.util.ArrayList;
import java.util.List;

public class ParkingLot {

    private final List<ParkingTicket> ticketList;
    private int remainingCapacity;
    private final int totalCapacity;

    public ParkingLot() {
        this.remainingCapacity = 10;
        this.totalCapacity = 10;
        this.ticketList = new ArrayList<>();
    }

    public ParkingLot(int capacity){
        this.remainingCapacity = capacity;
        this.totalCapacity = capacity;
        this.ticketList = new ArrayList<>();
    }

    public ParkingTicket park(Car car) {
        if (remainingCapacity > 0){
            this.remainingCapacity -= 1;
            ParkingTicket parkingTicket = new ParkingTicket(car);
            ticketList.add(parkingTicket);
            return parkingTicket;
        } else
            throw new NoAvailablePositionException();
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (ticketList.contains(parkingTicket)){
            ticketList.remove(parkingTicket);
            remainingCapacity += 1;
            parkingTicket.setUsed(true);
            return parkingTicket.getCar();
        }else if (parkingTicket.isUsed()){
            throw new UnRecognizedParkingTicketException();
        }else {
            throw new UnRecognizedParkingTicketException();
        }
    }

    public List<ParkingTicket> getTicketList() {
        return ticketList;
    }

    public int getRemainingCapacity() {
        return remainingCapacity;
    }

    public void setRemainingCapacity(int remainingCapacity) {
        this.remainingCapacity = remainingCapacity;
    }

    public int getTotalCapacity() {
        return totalCapacity;
    }
}
