package com.parkinglot;

import com.parkinglot.Exception.NoAvailablePositionException;
import com.parkinglot.Exception.UnRecognizedParkingTicketException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParkingLotServiceManager extends ParkingBoy{
    private List<ParkingBoy> parkingBoyList;

    public ParkingLotServiceManager(ParkingLot... parkingLots) {
        super(parkingLots);
        this.parkingBoyList = new ArrayList<>();
    }

    public void addParkingBoy(ParkingBoy... parkingBoys) {
        this.parkingBoyList.addAll(Arrays.asList(parkingBoys));
    }

    public ParkingTicket specifyBoyPark(ParkingBoy parkingBoy, Car car) throws NoAvailablePositionException {
        return parkingBoy.park(car);
    }

    public Car specifyBoyFetch(ParkingBoy parkingBoy, ParkingTicket parkingTicket) throws UnRecognizedParkingTicketException{
        return parkingBoy.fetch(parkingTicket);
    }

    public List<ParkingBoy> getParkingBoyList() {
        return parkingBoyList;
    }
}
