package com.parkinglot;

import java.util.Arrays;

public class ParkingBoy{
    private ParkingLot[] parkingLots;

    public ParkingBoy(ParkingLot... parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingLot[] getParkingLot() {
        return parkingLots;
    }

    public void setParkingLot(ParkingLot[] parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingTicket park(Car car) {
        return Arrays.stream(parkingLots).filter(parkingLot -> parkingLot.getRemainingCapacity() > 0).
                findFirst().orElse(parkingLots[0]).park(car);
    }

    public Car fetch(ParkingTicket parkingTicket) {
        //return this.parkingLot.fetch(parkingTicket);
        return Arrays.stream(parkingLots).filter(parkingLot -> parkingLot.getTicketList().contains(parkingTicket)).
                findFirst().orElse(parkingLots[0]).fetch(parkingTicket);
    }
}
